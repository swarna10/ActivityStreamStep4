package com.stackroute.activitystream.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.stackroute.activitystream.aspect.DAOLoggingAspect;
import com.stackroute.activitystream.aspect.LoggingAspect;
import com.stackroute.activitystream.model.Circle;
import com.stackroute.activitystream.model.Message;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages="com.stackroute")
@EnableWebMvc
@EnableAspectJAutoProxy
public class ApplicationContextConfig {
	@Bean(name="dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource= new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");

		dataSource.setUrl("jdbc:mysql://localhost:3306/"+System.getenv("MYSQL_DATABASE"));
		dataSource.setUsername(System.getenv("MYSQL_USER"));
		dataSource.setPassword(System.getenv("MYSQL_PASSWORD"));
		return dataSource;
	}
	
	private Properties getHibernateProperties() {
		Properties properties= new Properties();
		properties.put("hibernate.show_sql","true");
		properties.put("hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");
		properties.put("hibernate.hbm2ddl.auto", "update");
		return properties;
	}
	@Autowired
	@Bean(name="sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder sessionFactoryBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionFactoryBuilder.addProperties(getHibernateProperties());
		sessionFactoryBuilder.scanPackages("com.stackroute.activitystream.model");
		return sessionFactoryBuilder.buildSessionFactory();
	}
	
	@Autowired
	@Bean(name="transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager= new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}
	
	@Bean
	public DAOLoggingAspect daologingAspect() 
	{
		return new DAOLoggingAspect();
	}
	@Bean
	public LoggingAspect loggingAspect() 
	{
		return new LoggingAspect();
	}
	

}
